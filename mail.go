package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
)

const (
	//MaxUrlLen if url length is larger then content won't be requested
	MaxUrlLen     = 200
	MaxRoutines   = 5
	SleepDuration = 10 * time.Millisecond
)

func main() {
	r := bufio.NewReader(os.Stdin)
	s := bufio.NewScanner(r)
	//MaxScanTokenSize = 64 * 1024 so
	buf := make([]byte, 0, MaxUrlLen)
	s.Buffer(buf, MaxUrlLen)
	routines := make(chan struct{}, MaxRoutines)
	urls := make(chan string)
	goAmounts := make(chan int)
	finished := make(chan struct{})
	go func() {
		for s.Scan() {
			b := s.Bytes()
			//fmt.Println(string(b))
			urls <- string(b)
		}
		close(urls)
	}()

	go func(amounts <-chan int, done chan<- struct{}) {
		var total int
		for amount := range goAmounts {
			total += amount
		}
		fmt.Println("Total", total)
		done <- struct{}{}
	}(goAmounts, finished)

	for url := range urls {
		routines <- struct{}{}
		go func(url string, amounts chan<- int) {
			defer func() {
				<-routines
			}()
			resp, err := http.Get(url)
			if err != nil {
				log.Println(err)
				return
			}
			body, err := ioutil.ReadAll(resp.Body)
			resp.Body.Close()
			if err != nil {
				log.Println(err)
				return
			}
			bodyStr := string(body)
			gos := strings.Count(bodyStr, "Go")
			fmt.Println("The number of Go is", gos)
			amounts <- gos
			return
		}(url, goAmounts)
	}
	//Wait for remained routines
	for len(routines) > 0 {
		time.Sleep(SleepDuration)
	}
	close(goAmounts)
	<-finished
}
